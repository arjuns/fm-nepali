package com.kaagati;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Collection;

public class DataRowAdapter extends BaseAdapter  {

    private Context ctx;
    private final ArrayList<IDataRow> rows;

    public DataRowAdapter(Context ctx) {

        rows = new ArrayList<IDataRow>();
        this.ctx = ctx;
    }

    public void AddRow(IDataRow row) {

        this.rows.add(row);
    }
    public void AddManyRows(Collection<IDataRow> rows)
    {
        this.rows.addAll(rows);
    }

    public void RemoveRow(IDataRow row) {
        this.rows.remove(row);
    }


    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        return this.rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IDataRow row = rows.get(position);

        //NOTE:
        //convertView here serves us with no purpose at all, because we
        //have all different template for each DataRow, so recycled Template is of no use
        //SO, we should pass null to get the fresh one every time view is requested
        View view = row.GetView(this.ctx, null);
        return view;
    }

}
