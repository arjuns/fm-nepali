package com.kaagati.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.kaagati.FmStation;
import com.kaagati.R;
import com.kaagati.StationsAdaptor;
import com.kaagati.player.IRadioPlayerCallback;
import com.kaagati.player.RadioPlayer;


/**
 * Created by user on 3/6/2015.
 */
public class ShellFragment extends Fragment implements IRadioPlayerCallback {

    private RadioPlayer player;


    public ShellFragment() {
        int i=0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shell, container, false);
        ListView stationsView = (ListView) rootView.findViewById(R.id.lv_stations);
        stationsView.setAdapter(new StationsAdaptor(this));

        this.player = new RadioPlayer(getActivity(),this);
        return rootView;
    }

    @Override
    public void OnBeforePlay(FmStation station) {
        if (station != null) {

            station.OnStatusChange(FmStation.STATUS_BEFORE_PLAY);
        }
    }

    @Override
    public void OnAfterPlay(FmStation station) {
        if (station != null)
            station.OnStatusChange(FmStation.STATUS_AFTER_PLAY);
    }

    @Override
    public void OnBeforeStopped(FmStation station) {
        if (station != null)
            station.OnStatusChange(FmStation.STATUS_BEFORE_STOP);
    }

    @Override
    public void OnAfterStopped(FmStation station) {
        if (station != null)
            station.OnStatusChange(FmStation.STATUS_AFTER_STOP);
    }

    public FmStation getPlayingStation() {
        return this.player.getPlayingStation();
    }

    public void StopStation(FmStation station) {
        this.player.Stop();
    }

    public void PlayStation(FmStation station) {
        this.player.Play(station);
    }
}
