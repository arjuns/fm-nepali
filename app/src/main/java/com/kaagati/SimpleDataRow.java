package com.kaagati;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public abstract class SimpleDataRow implements IDataRow {

    private final int viewLayoutId;

    public SimpleDataRow(int viewLayoutId) {

        this.viewLayoutId = viewLayoutId;
    }

    @Override
    public View GetView(Context ctx, View cachedView) {

        View view = cachedView;
        if (view == null) {
            view = GetInflatedView(ctx);
        }
        InitializeView(view, ctx);
        return view;
    }

    ///write all the event hook code, view properties initializer here
    protected abstract void InitializeView(View view, Context ctx);


    protected View GetInflatedView(Context ctx) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        return inflater.inflate(this.viewLayoutId, null);

    }
}
