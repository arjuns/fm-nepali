package com.kaagati;

import android.app.Application;
import com.kaagati.network.Connectivity;

public class MainApp extends Application {

    static MainApp _app;
    public FmStation lasPlayingStation;

    public synchronized static MainApp Current() {
        return _app;
    }

    public Boolean IsConnectedToInternet() {
        return Connectivity.isConnected(this);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        _app = this;


    }


}
