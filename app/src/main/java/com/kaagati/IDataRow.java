package com.kaagati;

import android.content.Context;
import android.view.View;

/**
 * Created by user on 3/6/2015.
 */
public interface IDataRow {
    View GetView(Context ctx, View cachedView);
}
