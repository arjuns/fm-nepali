package com.kaagati.player;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import com.kaagati.FmStation;

import java.io.IOException;

/**
 * Created by user on 3/6/2015.
 */
public class RadioPlayer implements MediaPlayer.OnPreparedListener {


    private class DummyCallback implements IRadioPlayerCallback {
        @Override
        public void OnBeforePlay(FmStation station) {

        }

        @Override
        public void OnAfterPlay(FmStation station) {

        }

        @Override
        public void OnBeforeStopped(FmStation station) {

        }

        @Override
        public void OnAfterStopped(FmStation station) {

        }
    }


    private FmStation lastPlaying = null;
    private MediaPlayer player;
    private Activity mainActivity;
    private IRadioPlayerCallback callback;


    public FmStation getPlayingStation() {
        return this.lastPlaying;
    }

    public RadioPlayer(Activity mainActivity, IRadioPlayerCallback callback) {
        this.mainActivity = mainActivity;
        this.callback = callback;
        if (this.callback == null) {
            this.callback = new DummyCallback();
        }
    }

    public void Play(final FmStation station) {
        Stop();
        this.lastPlaying = station;
        callback.OnBeforePlay(station);

        this.player = new MediaPlayer();
        this.player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {


            this.player.setDataSource(station.StreamingUrl);
            this.player.setOnPreparedListener(this);
            this.player.prepareAsync();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void RunOnUIThread(Runnable runnable) {
        this.mainActivity.runOnUiThread(runnable);

    }


    public void Stop() {

        RunOnUIThread(new Runnable() {
            @Override
            public void run() {
                callback.OnBeforeStopped(lastPlaying);
            }
        });

        if (player != null) {
            this.player.release();
        }
        this.player = null;

        RunOnUIThread(new Runnable() {
            @Override
            public void run() {
                callback.OnAfterStopped(lastPlaying);
            }
        });


        this.lastPlaying = null;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        this.player.start();

        RunOnUIThread(new Runnable() {
            @Override
            public void run() {
                callback.OnAfterPlay(lastPlaying);
            }
        });


    }
}
