package com.kaagati.player;

import com.kaagati.FmStation;

public interface IRadioPlayerCallback {
    void OnBeforePlay(FmStation station);

    void OnAfterPlay(FmStation station);

    void OnBeforeStopped(FmStation station);

    void OnAfterStopped(FmStation station);

}
