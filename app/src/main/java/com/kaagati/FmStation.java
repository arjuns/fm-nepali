package com.kaagati;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.kaagati.activities.ShellFragment;


public class FmStation extends SimpleDataRow implements View.OnClickListener {

    public static final int STATUS_BEFORE_STOP = 0x41;
    public static final int STATUS_AFTER_STOP = 0x42;
    public static final int STATUS_BEFORE_PLAY = 0x43;
    public static final int STATUS_AFTER_PLAY = 0x44;


    public String Name;
    public String OperatingFrequency;
    public String StreamingUrl;
    private ShellFragment fragment;
    Button commandButton;
    TextView tv_status;
    TextView tv_station_name;

    public FmStation(ShellFragment fragment) {
        super(R.layout.layout_station);
        this.fragment = fragment;


    }


    @Override
    protected void InitializeView(View view, Context ctx) {
        this.tv_station_name = ((TextView) view.findViewById(R.id.tv_station_name));
        tv_station_name.setText(Name);
        ((TextView) view.findViewById(R.id.tv_station_frequency)).setText(OperatingFrequency);

        if (this.tv_status == null)
            tv_status = (TextView) view.findViewById(R.id.tv_station_status);
        commandButton = (Button) view.findViewById(R.id.btn_play_or_pause);
        commandButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        FmStation station = fragment.getPlayingStation();
        if (this.equals(station)) {
            this.fragment.StopStation(this);
        } else {
            this.fragment.PlayStation(this);
        }

    }

    public void OnStatusChange(int status) {

        if (status == STATUS_AFTER_PLAY) {

            this.tv_status.setText("PLAYING...");
            this.commandButton.setText("STOP");
        }
        if (status == STATUS_BEFORE_STOP) {
            this.tv_status.setText("STOPING....");
        }
        if (status == STATUS_BEFORE_PLAY) {
            this.fragment.getActivity().runOnUiThread(new Runnable() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void run() {
                    tv_status.setText("STARTING...");
                }
            });

            this.tv_status.invalidate();
        }
        if (status == STATUS_AFTER_STOP) {
            this.tv_status.setText("");
            this.commandButton.setText("PLAY");
        }

    }
}
