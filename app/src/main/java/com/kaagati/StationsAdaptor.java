package com.kaagati;

import android.app.Application;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;

import com.kaagati.activities.ShellFragment;
import com.kaagati.network.Connectivity;

/**
 * Created by user on 3/6/2015.
 */
public class StationsAdaptor extends DataRowAdapter {


    private ShellFragment fragment;

    public StationsAdaptor(ShellFragment fragment) {
        super(fragment.getActivity());
        this.fragment = fragment;

        AddDefaultStations();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        FmStation station = (FmStation) getItem(position);
        return station.GetView(this.fragment.getActivity(), null);

    }

    private void AddDefaultStations() {
        FmStation kaalika = new FmStation(this.fragment);
        kaalika.Name = "Kaalika Fm";
        kaalika.OperatingFrequency="95.2 Mz";
        kaalika.StreamingUrl = "http://kalika-stream.softnep.com:7740/";
        this.AddRow(kaalika);

        /* FmStation nepal = new FmStation(this.fragment);
        nepal.Name = "Nepal Fm";
        nepal.StreamingUrl = "http://kalika-stream.softnep.com:7740/";
        this.AddRow(nepal);*/
    }


}

